import { Component, OnInit, DoCheck } from '@angular/core';

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})
export class OneComponent implements OnInit, DoCheck {

  private test:boolean = true;

  //Primeio a ser executado. Sempre.
  constructor() { 
    console.log("Executando construtor.");
  }

  /*Executado quando o componente é instanciado.
  No Angular, quando ele é "montado em um 
  componente container". Logo após o construtor. */
  ngOnInit() {
    console.log("Executando ngOnInit.");
  }

  /*Sempre é executado quando há uma mudança no componente. 
  Qualquer mudança!
  Por exemplo, ao executar o método execute(), como o mesmo
  altera o valor de uma variavel do componente, o ngDoCheck
  será executado.*/
  ngDoCheck(){
    console.log("Executando ngDoCheck: ", this.test);
  }

  public execute(){
    this.test = !this.test;
  }


}
