
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  imgSrc:string = "assets/img/life-events.jpg";

  constructor() { }

  ngOnInit() {
  }

}
