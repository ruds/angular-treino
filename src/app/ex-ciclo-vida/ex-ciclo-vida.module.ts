import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrincipalComponent } from "./principal/principal.component";
import { OneComponent } from "./one/one.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PrincipalComponent,
    OneComponent
  ],
  exports:[
    PrincipalComponent
  ]
})
export class ExCicloVidaModule { }