import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BowTwoComponent } from './bow-two.component';

describe('BowTwoComponent', () => {
  let component: BowTwoComponent;
  let fixture: ComponentFixture<BowTwoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BowTwoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BowTwoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
