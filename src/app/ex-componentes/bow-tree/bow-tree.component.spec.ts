import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BowTreeComponent } from './bow-tree.component';

describe('BowTreeComponent', () => {
  let component: BowTreeComponent;
  let fixture: ComponentFixture<BowTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BowTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BowTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
