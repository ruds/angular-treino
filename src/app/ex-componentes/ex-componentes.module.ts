import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AreaUm } from './area_um/area.um';
import { AreaDois } from './area_dois/area.dois';
import { BoxOneComponent } from './box-one/box-one.component';
import { BowTwoComponent } from './bow-two/bow-two.component';
import { BowTreeComponent } from './bow-tree/bow-tree.component';
import { ContainerComponentsComponent } from './container-components/container-components.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    AreaUm,
    AreaDois,
    BoxOneComponent,
    BowTwoComponent,
    BowTreeComponent,
    ContainerComponentsComponent
  ],
  exports: [
    AreaUm,
    AreaDois,
    BoxOneComponent,
    BowTwoComponent,
    BowTreeComponent
  ]
})
export class ExComponentesModule { }
