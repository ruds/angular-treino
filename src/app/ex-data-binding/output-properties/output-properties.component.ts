import { Component, OnInit, EventEmitter, Output } from '@angular/core';

 @Component({
  selector: 'app-output-properties',
  templateUrl: './output-properties.component.html',
  styleUrls: ['./output-properties.component.css']
})
export class OutputPropertiesComponent implements OnInit {

  constructor() { }

  /*
    Enquanto o @Input expõe uma váriavel do componente filho
    para que ela possa ter seu valor alterado pelo componente
    pai - ou seja, recebe valores provenientes do pai - 
    o @Output faz o caminho inverso.

    O @Output envia valores do componente filho para o componente
    pai. Para tal ele usa um objeto EventEmitter, que necessariamente
    deve estar atrelado a um evento de um elemento do template do
    componente filho (por exemplo um "<a></a>").
    O método emit() do objeto EventEmitter será chamado quando a 
    função que esta atrelada a um evento (um "click" por exemplo)
    de um elemento do template. Passamos valor que queremos enviar
    para o componente pai como parâmetro do método "emit()"[1], sabendo
    que, esse valor pode ser até mesmo um objeto complexo.
  */

  protected value:string = "Valor vindo do componente filho";
  
  /*
  O objeto emitValue por sua vez, deve estar atrelado a um
  event-binding no seletor do componente presente no componente
  pai para que o pai tenha conhecimento da existência desse
  EventEmitter. 
  Esse event-binding estará ligado a um método existente no
  componente pai, método esse que recebe como parâmentro um objeto
  $event que contem (implicitamente) os valores passados pelo
  componente filho.  
   */
  @Output() emitValue:EventEmitter<string> = new EventEmitter();


  ngOnInit() {

  }

  /*
  #1 Esse método esta atrelado a um evento (click) em um button
     no template desse componente. Ao ser disparado propaga o valor
     passado como parâmetro.
   */
  protected sendValue(){
    this.emitValue.emit(this.value);
  }

}