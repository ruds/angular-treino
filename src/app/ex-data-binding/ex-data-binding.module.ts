import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { InterpolationComponent } from './interpolation/interpolation.component';
import { PropBindingComponent } from './prop-binding/prop-binding.component';
import { TwoWayComponent } from './two-way/two-way.component';
import { EventBindingComponent } from './event-binding/event-binding.component';
import { BindingContainerComponent } from './binding-container/binding-container.component';
import { InputPropertiesComponent } from './input-properties/input-properties.component';
import { OutputPropertiesComponent } from './output-properties/output-properties.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    InterpolationComponent,
    PropBindingComponent,
    TwoWayComponent,
    EventBindingComponent,
    BindingContainerComponent,
    InputPropertiesComponent,
    OutputPropertiesComponent
  ],
  exports: [
    InterpolationComponent,
    PropBindingComponent,
    TwoWayComponent,
    EventBindingComponent
  ]
})
export class ExDataBindingModule { }
