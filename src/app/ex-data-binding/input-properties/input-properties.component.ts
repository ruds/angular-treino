import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-input-properties',
  templateUrl: './input-properties.component.html',
  styleUrls: ['./input-properties.component.css']
})
export class InputPropertiesComponent implements OnInit {

  /*
  Expondo variavel ao componente pai.
  Fazendo isso, a váriavel exposta esta apta
  a ter seu valor alterado pelo componente pai. 
  */
  @Input() text:string = "Texto no filho";

  constructor() { }

  ngOnInit() {
  }

}
