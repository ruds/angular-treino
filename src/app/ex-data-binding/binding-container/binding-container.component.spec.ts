import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BindingContainerComponent } from './binding-container.component';

describe('BindingContainerComponent', () => {
  let component: BindingContainerComponent;
  let fixture: ComponentFixture<BindingContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BindingContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BindingContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
