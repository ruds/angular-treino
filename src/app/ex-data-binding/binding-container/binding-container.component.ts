import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-binding-container',
  templateUrl: './binding-container.component.html',
  styleUrls: ['./binding-container.component.css']
})
export class BindingContainerComponent implements OnInit {

  protected textMain:string = "Valor no componente pai";
  protected textMainByChild:string = "Valor pai antes de receber valor filho";

  constructor() { }

  /*
  Método que recebe o valor emitido pelo
  @Output (atributo emitValue) do componente filho.
  Esse método esta ligado ao event-binding (emitValue)
  presente no seletor do componente filho.
  */
  protected receiveValuefromChild(value){
    this.textMainByChild = value;
  }  

  ngOnInit() {
  }

}
