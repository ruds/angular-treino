"use strict";

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interpolation',
  templateUrl: './interpolation.component.html',
  styleUrls: ['./interpolation.component.css']
})
export class InterpolationComponent implements OnInit {

  constructor() { }

  variableText:string = "Variable containing text.";

  getText(){
    return 'Text returned by function.';
  }

  getSum(){
    return `Operation returned by function: ${2 + 2}`;
  }

  ngOnInit() {
  }

}
