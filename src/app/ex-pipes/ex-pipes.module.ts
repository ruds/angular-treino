import { NgModule, LOCALE_ID } from '@angular/core';
import { CommonModule, registerLocaleData } from '@angular/common';
import { FormsModule } from '@angular/forms';

import localePt from '@angular/common/locales/pt';

import { PipesContainerComponent } from './pipes-container/pipes-container.component';
import { PrimeiroPipeComponent } from './primeiro-pipe/primeiro-pipe.component';
import { CamelCasePipe } from './camel-case.pipe';

import { SettingsService } from "./settings.service";
import { PipePuroComponent } from './pipe-puro/pipe-puro.component';
import { FiltroArrayPipe } from './filtro-array.pipe';
import { FiltroArrayImpuroPipe } from './filtro-array-impuro.pipe';
import { PipeImpuroComponent } from './pipe-impuro/pipe-impuro.component';
import { UsandoFiltrosCorretamenteComponent } from './usando-filtros-corretamente/usando-filtros-corretamente.component';
import { PipeAsyncComponent } from './pipe-async/pipe-async.component';
// import { settings } from 'cluster';

registerLocaleData(localePt,'pt');

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [
    PipesContainerComponent, 
    PrimeiroPipeComponent, 
    CamelCasePipe, PipePuroComponent, FiltroArrayPipe, FiltroArrayImpuroPipe, PipeImpuroComponent, UsandoFiltrosCorretamenteComponent, PipeAsyncComponent
  ], 
  providers:[
    /*{provide: LOCALE_ID, useValue:'pt'}*/
    SettingsService,
    {
      provide: LOCALE_ID, 
      deps:[SettingsService],
      useFactory:(settingsService)=>settingsService.getLocale()
    }

  ],  
  exports:[PipesContainerComponent]
})
export class ExPipesModule { }
