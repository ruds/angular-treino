import { Pipe, PipeTransform } from '@angular/core';

import { FiltroArrayPipe } from './filtro-array.pipe';

@Pipe({
  name: 'filtroArrayImpuro',
  pure:false //Torna o pipe impuro. Sim. É só isso...
})

//Estamos herdando o método transform() de FiltroArrayPipe.
export class FiltroArrayImpuroPipe extends FiltroArrayPipe {}
