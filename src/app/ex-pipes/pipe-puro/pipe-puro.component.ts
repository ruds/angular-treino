import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-puro',
  templateUrl: './pipe-puro.component.html',
  styleUrls: ['./pipe-puro.component.css']
})
export class PipePuroComponent implements OnInit {

  filtro:string; 

  books: Array<string> = ['Java', 'Node', 'PHP'];

  constructor() { }

  ngOnInit() {
  }

  addLivro(value):void{
    this.books.push(value);
  }

}
