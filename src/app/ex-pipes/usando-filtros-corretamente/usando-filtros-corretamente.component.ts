import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-usando-filtros-corretamente',
  templateUrl: './usando-filtros-corretamente.component.html',
  styleUrls: ['./usando-filtros-corretamente.component.css']
})
export class UsandoFiltrosCorretamenteComponent implements OnInit {

  filtro:string; 

  books: Array<string> = ['Java', 'Node', 'PHP'];

  constructor() { }

  ngOnInit() {
  }

  addLivro(value):void{
    this.books.push(value);
  }

  obterLivros(){

    if(this.books.length===0||this.filtro===undefined||this.filtro.trim()==='')
      return this.books;

    let filter = this.filtro.toLocaleLowerCase();

    return this.books.filter(
      data=>{
        if(data.toLocaleLowerCase().indexOf(filter)>=0) return true;
        return false;
      }
    ); 
 
  }

}
