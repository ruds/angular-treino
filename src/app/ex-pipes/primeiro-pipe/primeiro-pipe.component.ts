import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-primeiro-pipe',
  templateUrl: './primeiro-pipe.component.html',
  styleUrls: ['./primeiro-pipe.component.css']
})
export class PrimeiroPipeComponent implements OnInit {

  book:any = {
     title:'Learning Angular',
     rating:'4.52345',
     pagesQtd:'320',
     price:'52.34',
     release: new Date(1989,5,16),
     url: 'http://a.co/glqjpRP'
  };

  constructor() { }

  ngOnInit() {
  }

}
