import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-impuro',
  templateUrl: './pipe-impuro.component.html',
  styleUrls: ['./pipe-impuro.component.css']
})
export class PipeImpuroComponent implements OnInit {


  filtro:string; 

  books: Array<string> = ['Java', 'Node', 'PHP'];

  constructor() { }

  ngOnInit() {
  }

  addLivro(value):void{
    this.books.push(value);
  }

}
