import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-pipe-async',
  templateUrl: './pipe-async.component.html',
  styleUrls: ['./pipe-async.component.css']
})
export class PipeAsyncComponent implements OnInit {

  filtro:string; 
  
  asyncValue = new Promise((res, rej)=>{
    setTimeout(() =>res('Valor do objeto assíncrono'), 2000);
  });  

  books: Array<string> = ['Java', 'Node', 'PHP'];

  constructor() { }

  ngOnInit() {
  }

  addLivro(value):void{
    this.books.push(value);
  }

  obterLivros(){
    if(this.books.length===0||this.filtro===undefined||this.filtro.trim()==='')
      return this.books;

    let filter = this.filtro.toLocaleLowerCase();

    return this.books.filter(
      data=>{
        if(data.toLocaleLowerCase().indexOf(filter)>=0) return true;
        return false;
      }
    ); 
  }

  
}
