import { Component, OnInit } from '@angular/core';

import { CursosService } from "../cursos.service";

@Component({
  selector: 'app-recebe-curso-criado',
  templateUrl: './recebe-curso-criado.component.html',
  styleUrls: ['./recebe-curso-criado.component.css'],
  providers:[CursosService]

})
export class RecebeCursoCriadoComponent implements OnInit {

  curso:string = 'Nenhum';

  constructor() { }

  ngOnInit() {
    CursosService
      .cursoAdicionado.subscribe(data=>{
        this.curso = data;
      });
  }

}
