import { Injectable, EventEmitter } from '@angular/core';

import { LogService } from "./log.service";

@Injectable({
  providedIn: 'root'
})
export class CursosService {

  private cursos:Array<string> = ["Java", "Node", "C++"];
  public static cursoAdicionado = new EventEmitter<string>();

  constructor(private logService:LogService) { console.log('CursosService'); }

  public getCursos():Array<string> {
    this.logService.logar('Obtendo listagem de cursos');
    return this.cursos;
  }

  public addCurso(curso:string):void {
    this.logService.logar(`Adicionando curso: ${curso}`);
    this.cursos.push(curso);
    CursosService.cursoAdicionado.emit(curso);
  }
}
