import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ServicesContainerComponent } from './services-container/services-container.component';
import { CursosComponent } from './cursos/cursos.component';
import { CriarCursoComponent } from './criar-curso/criar-curso.component';
import { RecebeCursoCriadoComponent } from './recebe-curso-criado/recebe-curso-criado.component';

import { CursosService } from "./cursos.service";
import { LogService } from "./log.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [ServicesContainerComponent, CursosComponent, CriarCursoComponent, RecebeCursoCriadoComponent],
  providers:[LogService],
  exports:[
    CursosComponent
  ]
})
export class ExServicesModule { }
