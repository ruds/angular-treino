import { Component, OnInit } from '@angular/core';

import { CursosService } from '../cursos.service';

@Component({
  selector: 'app-criar-curso',
  templateUrl: './criar-curso.component.html',
  styleUrls: ['./criar-curso.component.css'],
  providers:[CursosService],
})
export class CriarCursoComponent implements OnInit {

  cursos:Array<string> = [];

  constructor(private cursoService:CursosService) { }

  ngOnInit() {
    this.cursos = this.cursoService.getCursos();
  }

  public onAddCurso(curso:string):void{
    this.cursoService.addCurso(curso);
  }

}
