import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  protected listener:string = "Escutando...";

  constructor() { }

  ngOnInit() {
  }

  public listenerChangeName(value:any):void{
    this.listener = `Mudou para ${value}!`;
  }

}
