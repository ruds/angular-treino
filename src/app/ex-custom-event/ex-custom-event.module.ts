import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainComponent } from "./main/main.component";
import { CustomContainerComponent } from "./custom-container/custom-container.component";
import { ChildComponent } from "./child/child.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    MainComponent,
    CustomContainerComponent,
    ChildComponent
  ],
  exports:[
    CustomContainerComponent
  ]
})
export class ExCustomEventModule { }
