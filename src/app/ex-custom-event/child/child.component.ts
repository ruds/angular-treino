import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  protected name:string = 'Bené'
  
  @Output() 
  public changedName = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  public changeName():void{
    this.name = "Barbosa";
    this.changedName.emit(this.name);
  }

}
