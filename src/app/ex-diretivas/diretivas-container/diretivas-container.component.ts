import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-diretivas-container',
  templateUrl: './diretivas-container.component.html',
  styleUrls: ['./diretivas-container.component.css']
})
export class DiretivasContainerComponent implements OnInit {

  text:string = `Esse texto vem do 
                 component container
                 via 'ng-content'.`;

  constructor() { }

  ngOnInit() {
  }

}
