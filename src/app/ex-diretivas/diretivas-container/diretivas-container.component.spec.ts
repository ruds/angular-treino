import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiretivasContainerComponent } from './diretivas-container.component';

describe('DiretivasContainerComponent', () => {
  let component: DiretivasContainerComponent;
  let fixture: ComponentFixture<DiretivasContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiretivasContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiretivasContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
