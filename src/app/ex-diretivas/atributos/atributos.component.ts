import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-atributos',
  templateUrl: './atributos.component.html',
  styleUrls: ['./atributos.component.css']
})
export class AtributosComponent implements OnInit {

  amarelo:boolean = false;
  azul:boolean = true;
  verde:boolean = true;

  private change:boolean = true;
  constructor() { }

  ngOnInit() {
  }

  public changeColor():void{
    this.amarelo = !this.amarelo;
    this.azul = !this.azul;
    this.verde = !this.verde;
  }

}
