import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-estruturais',
  templateUrl: './estruturais.component.html',
  styleUrls: ['./estruturais.component.css']
})
export class EstruturaisComponent implements OnInit {

  cont:number = 0;
  valor:number = 0;
  a:number = 2;
  b:number = 1;
  mostrar:boolean = true;
  items:Array<number> = [];

  constructor() {}

  ngOnInit() {}

  public changeValue():void{
    let temp:number;
    temp = this.a;
    this.a = this.b;
    this.b = temp;
    this.mostrar = !this.mostrar;
  }

  public changeValueMethod():boolean{
    return !this.mostrar;
  }

  public incrementValue():void{
    if(this.valor<5) this.valor++;
    else this.valor=0;
  }

  public add():void{
    if(this.cont<4)
      this.items.push(this.cont++);
  }

  public remove():void{
    this.items.pop();
    if(this.items.length==0)
      this.cont = 0;
  }

}
