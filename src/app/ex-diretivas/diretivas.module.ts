import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { EstruturaisComponent } from './estruturais/estruturais.component';
import { DiretivasContainerComponent } from './diretivas-container/diretivas-container.component';
import { AtributosComponent } from './atributos/atributos.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [EstruturaisComponent, DiretivasContainerComponent, AtributosComponent],
  exports:[EstruturaisComponent]
})
export class DiretivasModule { }
