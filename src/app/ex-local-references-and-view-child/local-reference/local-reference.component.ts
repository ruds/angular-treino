import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-local-reference',
  templateUrl: './local-reference.component.html',
  styleUrls: ['./local-reference.component.css'],
  /* 
  ViewEncapsulation.None: Utilizado para informar que o CSS
  desse componente também influenciará os componentes filhos.
  */
  encapsulation: ViewEncapsulation.None
})
export class LocalReferenceComponent implements OnInit {

  msg:string = "Olá!";

  constructor() { }

  ngOnInit() { }


  /*Recebendo as referências de elementos do template
   via parâmetros. Foram declaradas variaveis no 
   template (#nomeVariavel) e as mesmas foram passadas
   por parâmetros na chamada dos métodos que estão
   atrelados a event-bindings.*/
  public execute(inputText:HTMLInputElement, 
    msgElement:HTMLParagraphElement):void{
    this.msg = inputText.value||'Is empty!';
    inputText.value = "";
    msgElement.classList.remove('msg-warn');    
    msgElement.classList.add('msg-ok');
  }

  public changeToWarning(msgElement:HTMLParagraphElement):void{
    this.msg = 'Warning!';
    msgElement.classList.remove('msg-ok');
    msgElement.classList.add('msg-warn');
  }

}