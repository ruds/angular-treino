import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LocalReferenceComponent } from "./local-reference/local-reference.component";
import { ViewChildComponent } from "./view-child/view-child.component";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LocalReferenceComponent,
    ViewChildComponent
  ],
  exports:[LocalReferenceComponent]
})
export class ExLocalReferencesAndViewChildModule { }
