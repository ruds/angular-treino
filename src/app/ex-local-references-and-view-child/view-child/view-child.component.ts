import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-view-child',
  templateUrl: './view-child.component.html',
  styleUrls: ['./view-child.component.css'],
})
export class ViewChildComponent implements OnInit {

  msg:string = "Olá!";
  
  /*Utilizamos o @ViewChild para acessar diretamente
  o DOM do template de um @Component.
  
  Colocamos o decorator @ViewChild no atributo que
  desejamos que referencie o elemento do template 
  na classe do componente, passando como parâmetro 
  para o construtor  do decorator o nome da variavel 
  criada formulário (#nomeVariavel) para representar
  o elemento a ser acessado.*/
  @ViewChild('msgElement')
  paragrafo:ElementRef;


  @ViewChild('inputTexto')
  inputTexto:ElementRef;
  
  constructor() { }

  ngOnInit() { }


  /*Acessando e alterando alguns valores do DOM diretamente
  utilizando os atributos decorados com @ViewChild.
  Também alteramos alguns atributos, como por exemplo, o 
  atributo classList dos elementos HTML referenciados.*/
  public execute():void{
    this.msg = this.inputTexto.nativeElement.value||'Is empty!';
    this.inputTexto.nativeElement.value = "";
    this.paragrafo.nativeElement.classList.remove('msg-warn');    
    this.paragrafo.nativeElement.classList.add('msg-ok');
  }

  public changeToWarning():void{
    this.msg = 'Warning!';
    this.paragrafo.nativeElement.classList.remove('msg-ok');
    this.paragrafo.nativeElement.classList.add('msg-warn');
  }

}