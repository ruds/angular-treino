import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { Routing } from "./app.routes";

import { DiretivasModule } from "./ex-diretivas/diretivas.module";
import { ExComponentesModule } from "./ex-componentes/ex-componentes.module";
import { ExDataBindingModule } from "./ex-data-binding/ex-data-binding.module";
import { ExCicloVidaModule } from "./ex-ciclo-vida/ex-ciclo-vida.module";
import { ExCustomEventModule } from "./ex-custom-event/ex-custom-event.module";
import { ExLocalReferencesAndViewChildModule }
  from "./ex-local-references-and-view-child/ex-local-references-and-view-child.module";
import { ExCustomDirectivesModule } from "./ex-custom-directives/ex-custom-directives.module";
import { ExServicesModule } from './ex-services/ex-services.module';
import { ExPipesModule } from "./ex-pipes/ex-pipes.module";

import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    Routing,
    DiretivasModule,
    ExComponentesModule,
    ExDataBindingModule,
    ExCicloVidaModule,
    ExCustomEventModule,
    ExLocalReferencesAndViewChildModule,
    ExCustomDirectivesModule,
    ExServicesModule,
    ExPipesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }