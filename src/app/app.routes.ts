"use strict";

import { Routes, RouterModule } from "@angular/router";

import { DiretivasContainerComponent } from "./ex-diretivas/diretivas-container/diretivas-container.component";
import { ContainerComponentsComponent } from "./ex-componentes/container-components/container-components.component";
import { BindingContainerComponent } from "./ex-data-binding/binding-container/binding-container.component";
import { PrincipalComponent } from './ex-ciclo-vida/principal/principal.component';
import { CustomContainerComponent } from "./ex-custom-event/custom-container/custom-container.component";
import { LocalReferenceComponent } 
    from './ex-local-references-and-view-child/local-reference/local-reference.component';
import { CustomDirectivesComponent } from "./ex-custom-directives/custom-directives/custom-directives.component";
import { ServicesContainerComponent } from "./ex-services/services-container/services-container.component";
import { PipesContainerComponent } from "./ex-pipes/pipes-container/pipes-container.component";


const appRoutes: Routes = [
    {path:'diretivas', component: DiretivasContainerComponent},
    {path:'componentes', component: ContainerComponentsComponent},
    {path:'binding', component: BindingContainerComponent},
    {path:'life', component: PrincipalComponent},
    {path:'custom-event', component: CustomContainerComponent},
    {path:'references', component: LocalReferenceComponent},
    {path:'custom-directive', component: CustomDirectivesComponent},
    {path:'services', component: ServicesContainerComponent},
    {path:'pipes', component: PipesContainerComponent},    
    {path:'**', redirectTo: ''}
];

export const Routing = RouterModule.forRoot(appRoutes);