import { Directive, ElementRef, Renderer } from '@angular/core';

@Directive({
  /*É possivel especificar o tipo de elemento onde
  será possivel aplicar a diretiva customizada. Para tal,
  informa-se a tag do elemento antes do seletor da diretiva.*/
  selector: 'p[appFundoAmarelo]'
})
export class FundoAmareloDirective {

  constructor(
    private _elementRef:ElementRef,
    private _render:Renderer) {

    /*Evitar acessar elemento diretamente pelo ElementRef pois o mesmo acarreta
    problemas de segurança do tipo XXS. Ao invés disso utilizar o Renderer[depreciado!].*/
    // this._elementRef.nativeElement.style.backgroundColor = 'yellow';
    this._render.setElementStyle(this._elementRef.nativeElement, 'background-color','yellow');
  
  }

}
