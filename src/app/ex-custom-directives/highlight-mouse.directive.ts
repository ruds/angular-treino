import { Directive, HostListener, ElementRef, Renderer, HostBinding } from '@angular/core';

@Directive({
  selector: '[appHighlightMouse]'
})
export class HighlightMouseDirective {
  
  @HostBinding('style.backgroundColor') backgroundColor:string;
  
  constructor() {}


  @HostListener('mouseenter')
  mouseSobre():void{
    this.backgroundColor = 'red';
  }

  @HostListener('mouseleave')
  mouseSaiu():void{
    this.backgroundColor = 'green';
  }



}
