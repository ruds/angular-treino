import { Directive, HostListener, HostBinding, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective implements OnInit{

  @HostBinding('style.backgroundColor') backgroundColor:string;
  
  @Input('default') deafaultColor:string = 'white';
  @Input('high') highlightColor:string = 'yellow';

  constructor() {}

  ngOnInit(){
    this.backgroundColor = this.deafaultColor;
  }

  @HostListener('mouseenter')
  mouseSobre():void{
    this.backgroundColor = this.highlightColor;
  }

  @HostListener('mouseleave')
  mouseSaiu():void{
    this.backgroundColor = this.deafaultColor;
  }

}