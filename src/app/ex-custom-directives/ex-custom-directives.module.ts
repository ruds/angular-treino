import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FundoAmareloDirective } from './fundo-amarelo.directive';
import { CustomDirectiveAtributeComponent } from './custom-directive-atribute/custom-directive-atribute.component';
import { CustomDirectivesComponent } from './custom-directives/custom-directives.component';
import { HostListenerBindingComponent } from './host-listener-binding/host-listener-binding.component';
import { HighlightMouseDirective } from './highlight-mouse.directive';
import { InputPropertyBindingComponent } from './input-property-binding/input-property-binding.component';
import { HighlightDirective } from './highlight.directive';
import { CustomDirectiveEstructureNgElseComponent } from './custom-directive-estructure-ng-else/custom-directive-estructure-ng-else.component';
import { NgElseDirective } from './ng-else.directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    FundoAmareloDirective,
    CustomDirectiveAtributeComponent,
    CustomDirectivesComponent,
    HostListenerBindingComponent,
    HighlightMouseDirective,
    InputPropertyBindingComponent,
    HighlightDirective,
    CustomDirectiveEstructureNgElseComponent,
    NgElseDirective
  ],
  exports:[
    CustomDirectivesComponent
  ]
})
export class ExCustomDirectivesModule { }
