import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-host-listener-binding',
  templateUrl: './host-listener-binding.component.html',
  styleUrls: ['./host-listener-binding.component.css']
})
export class HostListenerBindingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
