import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-custom-directive-estructure-ng-else',
  templateUrl: './custom-directive-estructure-ng-else.component.html',
  styleUrls: ['./custom-directive-estructure-ng-else.component.css']
})
export class CustomDirectiveEstructureNgElseComponent implements OnInit {

  mostrarCursos:boolean = false; 

  constructor() { }

  ngOnInit() {
  }

  onMostrarCursos():void{
    this.mostrarCursos = !this.mostrarCursos;
  }

}